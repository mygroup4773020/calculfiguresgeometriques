/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ioc.xtec.cat.calculfiguresgeometriques;

import java.util.Scanner;

/**
 * Aquesta classe representa un rectangle.
 */
public class Rectangle implements FiguraGeometrica {
    private final double base;
    private final double altura;
    
    /**
     * Constructor per crear un nou rectangle.
     */
    public Rectangle() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introdueix la longitud de la base del rectangle: ");
        this.base = scanner.nextDouble();
        System.out.println("Introdueix la longitud de l'altura del rectangle: ");
        this.altura = scanner.nextDouble();
    }
    
    /**
     * Calcula l'àrea del rectangle.
     * @return L'àrea del rectangle.
     */
    @Override
    public double calcularArea() {
        return base * altura;
    }
    
    /**
     * Calcula el perímetre del rectangle.
     * @return El perímetre del rectangle.
     */
    @Override
    public double calcularPerimetre() {
        return 2 * (base + altura);
    }
}

